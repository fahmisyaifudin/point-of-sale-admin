const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/app.js', 'public/assets/js')
mix.sass('resources/css/app.scss', 'public/assets/css')

mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/assets/webfonts');
mix.copy('node_modules/bootstrap-fileinput', 'public/assets/vendor/bootstrap-fileinput');

mix.styles([
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/@fortawesome/fontawesome-free/css/all.css',
    'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css',
], 'public/assets/css/vendor.css')
