<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        DB::table('users')->insert([
            'name' => 'Admin',
            'phone' => $faker->phoneNumber(),
            'shopName' => $faker->firstName().' Shop',
            'address' => $faker->address(),
            'email' => 'admin@example.com',
            'password' => Hash::make('password'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        for ($i=0; $i < 5; $i++) { 
            DB::table('cashiers')->insert([
                'user_id' => 1,
                'name' => $faker->name,
                'username' => $faker->userName,
                'password' => Hash::make('password'),
                'auth_key' => Str::random(32),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        for ($i=0; $i < 5; $i++) { 
            DB::table('products')->insert([
                'user_id' => 1,
                'name' => $faker->text(20),
                'price' => $faker->randomNumber(5),
                'stock' => $faker->randomNumber(2),
                'unit_name' => 'pcs',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        for ($i=0; $i < 25; $i++) { 
            $date = $faker->dateTimeThisMonth();
            $id = DB::table('transactions')->insertGetId([
                'cashier_id' => $faker->numberBetween(1, 5),
                'nominal' => $faker->randomNumber(5),
                'created_at' => $date,
                'updated_at' => $date
            ]);

            $n = $faker->numberBetween(1, 4);
            for ($j=0; $j < $n; $j++) { 
                DB::table('transaction_items')->insert([
                    'transaction_id' => $id,
                    'product_id' => $faker->numberBetween(1, 5),
                    'quantity' =>  $faker->numberBetween(1, 10),
                    'created_at' => $date,
                    'updated_at' => $date
                ]);
            }
        }
    }
}
