<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cashier extends Model
{
  use SoftDeletes;
  protected $guarded = [];
  protected $hidden = ['password', 'auth_key'];

  public function user()
  {
      return $this->belongsTo('App\Models\User');
  }

  public function transactions()
  {
      return $this->hasMany('App\Models\Transaction');
  }
}