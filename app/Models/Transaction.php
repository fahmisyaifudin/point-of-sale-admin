<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function cashier()
    {
        return $this->belongsTo('App\Models\Cashier');
    }

    public function items()
    {
        return $this->hasMany('App\Models\TransactionItem');
    }
}