<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class TransactionController extends Controller
{
    public function index(){
        return view('transaction/index');
    }
    
    public function table(){
        try {
            $id = session('session_userid');
            $trx = Transaction::whereHas('cashier', function(Builder $query) use ($id) {
                $query->where('user_id', $id);
            })->with(['cashier'])->get();
            return $this->successResponse($trx);
        }catch(\Exception $e){
            return $this->errorResponse($e, 500);
        }
    }

    public function detail($id){
        try {
            $trx = Transaction::where('id', $id)->with(['cashier', 'items', 'items.product'])->first();
            return $this->successResponse($trx);
        }catch(\Exception $e){
            return $this->errorResponse($e, 500);
        }
    }

    public function download(){
        $id = session('session_userid');
        $trx = Transaction::whereHas('cashier', function(Builder $query) use ($id) {
            $query->where('user_id', $id);
        })->with(['cashier','items', 'items.product'])->get();

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(100);

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Date')
            ->setCellValue('B1', 'Cashier')
            ->setCellValue('C1', 'Nominal')
            ->setCellValue('D1', 'Description');

        $spreadsheet->getActiveSheet()->getStyle('C1')->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $i = 2;
        foreach ($trx as $t) {
            $description = '';
            foreach ($t->items as $item) {
                $description .= $item->quantity.' '.$item->product->name.' ,';
            }

            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $t->created_at)
                ->setCellValue('B'.$i, $t->cashier->username)
                ->setCellValue('C'.$i, $t->nominal)->getStyle('C'.$i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('D'.$i, $description);
            
            $i++;
        }
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Transaction_export.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
   
