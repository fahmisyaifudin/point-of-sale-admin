<?php

namespace App\Http\Controllers;

use App\Models\Cashier;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class CashierController extends Controller
{
    public function index(){
        return view('cashier/index');
    }

    public function table(){
        $data = Cashier::where('user_id', session('session_userid'))->get();
        return response()->json(['data' => $data]);
    }

    public function detail($id){
        $data = Cashier::find($id);
        return response()->json(['data' => $data]);
    }

    public function create(Request $request) {
        $input = $this->validate($request, [
          'name' => 'required|string',
          'username' => 'required|string|min:6',
          'password' => 'required|string|confirmed|min:6'
        ]);

        try{
          $input['password'] = Hash::make($input['password']);
          $input['user_id'] = session('session_userid');
          $input['auth_key'] = Str::random(32);

          $pos = Cashier::create($input);

          return response()->json(['message' => 'success', 'data' => $pos]);

        }catch(\Exception $e){
            return response()->json(['message' => $e], 500);
        }
    }

    public function update(Request $request){

        $input = $this->validate($request, [
            'password' => 'required|string|confirmed|min:6'
        ]);

        try{
            $id = $request->id;
            $pos = Cashier::find($id);
            $pos->password = Hash::make($request->password);
            $pos->save();
    
            return response()->json(['message' => 'success', 'data' => $pos]);
          }catch(\Exception $e) {
            return response()->json(['message' => $e], 500);
          }
    }

    public function delete(Request $request){
        try {
          $id = $request->id;
          $pos = Cashier::find($id);
          $pos->delete();
  
          return $this->successResponse();
  
        } catch (\Exception $e) {
          return $this->errorResponse($e, 500);
        }
      }
  
}
