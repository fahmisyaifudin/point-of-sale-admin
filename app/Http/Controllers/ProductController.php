<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index(){
        return view('product/index');
    }

    public function table(){
        $data = Product::where('user_id', session('session_userid'))->get();
        return response()->json(['data' => $data]);
    }

    public function detail($id){
        $data = Product::find($id);
        return response()->json(['data' => $data]);
    }

    public function create(Request $request) {
        $input = $this->validate($request, [
            'name' => 'required|string',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'unit_name' => 'required|string'
         ]);
  
        try{
            $product = new Product();
            $product->user_id = session('session_userid');
  
            if ($request->hasFile('photo')) {
              $filename =  Str::random(32).'.'.$request->photo->extension();
              $path = $request->file('photo')->move('storage/products', $filename);
              $product->photo = 'storage/products'.'/'.$filename;
            } 
  
            $product->name = $input['name'];
            $product->price = $input['price'];
            $product->stock = $input['stock'];
            $product->unit_name = $input['unit_name'];
            $product->save();
  
            return $this->successResponse($product);
  
          }catch(\Exception $e){
            return $this->errorResponse($e, 500);
          }
    }

    public function update(Request $request){

      $this->validate($request, [
        'name' => 'required|string',
        'price' => 'required|numeric',
        'stock' => 'required|numeric',
        'unit_name' => 'required|string'
      ]);

      try{
        $product = Product::find($request->id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->unit_name = $request->unit_name;
        $product->stock = $request->stock;

        if ($request->hasFile('photo')) {
          if ($product->photo) {
            unlink($product->photo);
          }
          $filename =  Str::random(32).'.'.$request->photo->extension();
          $path = $request->file('photo')->move('storage/products', $filename);
          $product->photo = 'storage/products'.'/'.$filename;
        }

        $product->save();

        return $this->successResponse($product);
      }catch(\Exception $e) {
          return $this->errorResponse($e, 500);
      }
    }

    public function delete(Request $request){
        try {
          $id = $request->id;
          $pos = Product::find($id);
          $pos->delete();
  
          return $this->successResponse();
  
        } catch (\Exception $e) {
          return $this->errorResponse($e, 500);
        }
    }
}
