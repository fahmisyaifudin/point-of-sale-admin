<?php

namespace App\Http\Controllers;

use App\Models\Cashier;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Transaction;

class HomeController extends Controller
{
    public function index(){
        return view('home');
    }

    public function count(){
        try {
            $month = (int) date("m",strtotime("-1 month"));;
            $year = date('Y');

            $userid = session('session_userid');
            $data['cTrx'] = Transaction::whereHas('cashier', function(Builder $query) use ($userid) {
                $query->where('user_id', $userid);
            })->select(DB::raw('SUM(nominal) as total'))
                ->whereMonth('created_at', $month)
                ->whereyear('created_at', $year)
                ->first()->total;
        
            $data['cProduct'] = Product::where('user_id', $userid)->count();
            $data['cCashier'] = Cashier::where('user_id', $userid)->count();
            return $this->successResponse($data);
        }catch(\Exception $e){
            return $this->errorResponse($e, 500);
        }
    }

    public function chart(){
        try {
            $userid = session('session_userid');
            $data = Transaction::whereHas('cashier', function(Builder $query) use ($userid) {
                $query->where('user_id', $userid);
            })->select(DB::raw('SUM(nominal) as y'), DB::raw("DATE_FORMAT(created_at, '%m/%d/%Y') as x"))
             ->whereYear('created_at', date('Y'))
             ->groupBy(DB::raw("DATE_FORMAT(created_at, '%m/%d/%Y')"))->get();

             return $this->successResponse($data);
        }catch(\Exception $e){
            return $this->errorResponse($e, 500);
        }
    }
}
