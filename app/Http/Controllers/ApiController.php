<?php

namespace App\Http\Controllers;

use App\Models\Cashier;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    protected function jwt(Cashier $user) {
        $payload = [
            'iss' => "chrono-pos-jwt", // Issuer of the token
            'sub' => $user->auth_key, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }
    
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        try {
            $cashier = Cashier::where('username', $request->username)->with('user')->first();
            if (!$cashier) {
                return $this->errorResponse('Username does not exist', 400);
            }

            if (Hash::check($request->password, $cashier->password)) {
                return $this->successResponse(['user' => $cashier, 'token' => $this->jwt($cashier)]);  
            }
            
            return $this->errorResponse('Password or username wrong', 400);
        } catch (\Exception $e) {
            return $this->errorResponse($e, 500);
        }
    }

    public function getProduct(Request $request)
    {
        try {
            $cashier = Cashier::where('auth_key', $request->auth_key)->first();
            $product = Product::where('user_id', $cashier->user_id)->get();
            return $this->successResponse($product);
        } catch (\Exception $e) {
            return $this->errorResponse($e, 500);
        }  
    }

    public function createTransaction(Request $request)
    {
        $this->validate($request, [
            'item' => 'required|json'
        ]);

        try {
            $amount = 0;
            $items = json_decode($request->item);
            $cashier = Cashier::where('auth_key', $request->auth_key)->first();

            foreach ($items as $key => $item) {
                $amount += $item->price * $item->quantity;
            }

            $transaction = new Transaction();
            $transaction->cashier_id = $cashier->id;
            $transaction->nominal = $amount;
            $transaction->save();

            $transaction->item = $request->item;

            foreach ($items as $key => $item) {
               $trxItem = new TransactionItem();
               $trxItem->transaction_id = $transaction->id;
               $trxItem->product_id = $item->id;
               $trxItem->quantity = $item->quantity;
               $trxItem->save();
            }

            return $this->successResponse($transaction);
        } catch (\Exception $e) {
            return $this->errorResponse($e, 500);
        }
    }
}
