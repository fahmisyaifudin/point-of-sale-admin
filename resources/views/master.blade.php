<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title')</title>

  <!-- General CSS Files -->
  @yield('css')
  <link rel="stylesheet" href="/assets/css/app.css">
  <link rel="stylesheet" href="/assets/css/vendor.css">
  <link rel="stylesheet" href="/assets/css/global.css">
  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="/assets/templates/css/style.css">
  <link rel="stylesheet" href="/assets/templates/css/components.css">

</head>

<body>
  <div class="preloader">
      <div class="loading">
        <img src="/assets/img/poi.gif" width="80">
      </div>
  </div>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="/assets/img/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{ session('session_name') }}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="features-activities.html" class="dropdown-item has-icon">
                <i class="fas fa-bolt"></i> Activities
              </a>
              <a href="features-settings.html" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a>
              <div class="dropdown-divider"></div>
              <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      @include('sidebar')
      <!-- Main Content -->
      <div class="main-content">
            @yield('content')
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
        </div>
        <div class="footer-right">
          2.3.0
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="/assets/js/app.js"></script>
  <script src="/assets/templates/js/scripts.js"></script>
  <script src="/assets/templates/js/stisla.js"></script>
  <script>
    $(".preloader").fadeOut();
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    
    $( document ).ajaxStart(function() {
      // $(".preloader").fadeIn();
    });

    $( document ).ajaxSuccess(function( event, xhr, settings ) {
      if (settings.method == 'POST') {
        toastr.success('Success !')
      }
    });

    $( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
      if (jqxhr.status == 500) {
        toastr.error('Internal Server Error !')
      }else{
        toastr.error(jqxhr.responseJSON.message)
      }
    });

    $( document ).ajaxComplete(function() {
      // $(".preloader").fadeOut();
    });
    
  </script>
  @yield('js')

  <!-- Page Specific JS File -->
</body>
</html>
