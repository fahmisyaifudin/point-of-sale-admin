
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="index.html">POS Admin</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">POS</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item dropdown {{ (request()->is('/')) ? 'active' : '' }}">
                <a href="/" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Admin</li>
            <li class="nav-item dropdown {{ (request()->is('product')) ? 'active' : '' }}">
                <a href="{{ route('product') }}" class="nav-link"><i class="fas fa-shopping-cart"></i><span>Product</span></a>
            </li>
            <li class="nav-item dropdown {{ (request()->is('cashier')) ? 'active' : '' }}">
                <a href="{{ route('cashier') }}" class="nav-link"><i class="fas fa-user-friends"></i><span>Cashier</span></a>
            </li>
            <li class="nav-item dropdown {{ (request()->is('transaction')) ? 'active' : '' }}">
                <a href="{{ route('transaction') }}" class="nav-link"><i class="fas fa-exchange-alt"></i><span>Transaction</span></a>
            </li>
        </ul>

        
    </aside>
</div>