@extends('master')
@section('title', 'Transaction')


@section('content')
<section class="section">
    <div class="section-body">
        <div class="card">
            <div class="card-header">
                <h4>@yield('title')</h4>
            </div>
            <div class="card-body">
               <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="table-active" class="table" style="width: 100%;"></table>
                        </div>
                    </div>
                    <a href="{{ route('transaction.download') }}" class="btn btn-success mt-2 ml-3"><i class="fas fa-download mr-3"></i>Download</a>
               </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-item">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Date</label>
                        <input type="text" class="form-control" id="fDate" disabled>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Nominal</label>
                        <input type="text" class="form-control" id="fNominal" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Cashier Username</label>
                        <input type="text" class="form-control" id="fCashierUserName" disabled>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Cashier Name</label>
                        <input type="text" class="form-control" id="fCashierName" disabled>
                    </div>
                </div>
                <div class="row">
                    <table class="table">
                        <tr>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Total</th>
                        </tr>
                        <tbody id="detailTable">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
<script>
    const loadData = () => {
        $('#table-active').DataTable( {
                destroy: true,
                scroller: true,
                scrollX: 200,
                ajax: {
                    url: "<?= route('transaction.table') ?>",
                    method: "GET",
                },
                columns: [
                    {
                        data: "created_at",
                        title: "Date"
                    },
                    {
                        data: "cashier.username",
                        title: "Cashier"
                    },
                    {
                        data: "nominal",
                        title: "Nominal"
                    },
                    {
                        title: 'Action',
                        data: 'id'
                    }
                ],
                rowCallback: function(row, data) {
                    $('td:eq(0)', row).html(new Date(data['created_at']).toLocaleString());
                    $('td:eq(2)', row).html(data['nominal'].toLocaleString());
                    $('td:eq(3)', row).html(`
                        <a onClick="modalView(`+ data['id'] +`)"><i class="fas fa-eye text-primary"></i></a>
                    `);
                },
        } );
    }

    const modalView = (id) => {
        $.ajax({
            url: "<?= route('transaction') ?>" + "/detail/" + id,
        })
        .done((res) => {
            $('#modal-item').modal('show')
            $('#modal-title').html('View Transaction')

            $('#fDate').val(new Date(res.data.created_at).toLocaleString())
            $('#fNominal').val(res.data.nominal.toLocaleString())
            $('#fCashierUserName').val(res.data.cashier.username)
            $('#fCashierName').val(res.data.cashier.name)
            
            $("#detailTable").html('');
            let total = 0;
            res.data.items.forEach(item => {
                $("#detailTable").append(`<tr>
                            <td>` + item.product.name +`</td>
                            <td>` + item.quantity +` `+ item.product.unit_name +`</td>
                            <td>`+ item.product.price.toLocaleString() +`</td>
                            <td>`+ (item.product.price * item.quantity).toLocaleString() +`</td>
                        </tr>`)
                total += item.product.price * item.quantity;
            });
            $("#detailTable").append(`<tr>
                            <td></td><td></td><td></td>
                            <th>`+ total.toLocaleString() +`</th>
                        </tr>`)
        })
    }

    //Created
    loadData();
</script>

@endsection

