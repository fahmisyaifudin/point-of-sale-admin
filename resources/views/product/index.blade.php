@extends('master')
@section('title', 'Product')

@section('css')
<link rel="stylesheet" href="/assets/vendor/bootstrap-fileinput/css/fileinput.min.css">
@endsection

@section('content')
<section class="section">
    <div class="section-body">
        <div class="card">
            <div class="card-header">
                <h4>@yield('title')</h4>
            </div>
            <div class="card-body">
               <a onClick="modalAdd()" class="btn btn-primary mb-3">Add</a>
               <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="table-active" class="table" style="width: 100%;"></table>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-item">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="myForm">
            <div class="modal-body">
                <input type="text" class="form-control" name="id" id="fId" hidden>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" id="fName">
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <input type="text" class="form-control" name="price" id="fPrice">
                </div>
                <div class="form-group">
                    <label>Stock</label>
                    <input type="text" class="form-control" name="stock" id="fStock">
                </div>
                <div class="form-group">
                    <label>Unit Name</label>
                    <input type="text" class="form-control" name="unit_name" id="fUnitName">
                </div>
                <div class="form-group">
                    <label>Photo</label>
                    <input id="fPhoto" name="photo" type="file" class="file" data-browse-on-zone-click="true">
                </div>

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="https://cdn.rawgit.com/plentz/jquery-maskmoney/master/dist/jquery.maskMoney.min.js"></script>
<script src="/assets/vendor/bootstrap-fileinput/js/fileinput.min.js"></script>
<script>
    const loadData = () => {
        $('#table-active').DataTable( {
                destroy: true,
                scroller: true,
                scrollX: 200,
                ajax: {
                    url: "<?= route('product.table') ?>",
                    method: "GET",
                },
                columns: [
                    {
                        data: "name",
                        title: "Name"
                    },
                    {
                        data: "price",
                        title: "Price"
                    },
                    {
                        data: "stock",
                        title: "Stock"
                    },
                    {
                        data: "unit_name",
                        title: "Unit Name"

                    },
                    {
                        title: 'Action',
                        data: 'id'
                    }
                ],
                rowCallback: function(row, data) {
                    $('td:eq(1)', row).html(data['price'].toLocaleString());
                    $('td:eq(4)', row).html(`
                        <a onClick="modalEdit(`+ data['id'] +`)"><i class="fas fa-pen text-info"></i></a>
                        <a onClick="modalDelete(`+ data['id'] +`)"><i class="fas fa-trash text-danger"></i></a>
                    `);
                },
        } );
    }

    const modalAdd = () => {
        $('#modal-item').modal('show')
        $('#modal-title').html('Add Product')

        $('#fName').prop('disabled', false)
        $('#fPrice').prop('disabled', false)
        $('#fStock').prop('disabled', false)
        $('#fUnitName').prop('disabled', false)
        $("#fPhoto").fileinput('refresh');

        $('#fId').val(null)
        $('#fName').val(null)
        $('#fPrice').val(null)
        $('#fStock').val(null)
        $('#fUnitName').val(null)
    }

    const modalEdit = (id) => {
        $.ajax({
            url: "<?= route('product') ?>" + "/detail/" + id,
        })
        .done((res) => {
            $('#modal-item').modal('show')
            $('#modal-title').html('Edit Product')

            $('#fId').val(res.data.id)
            $('#fName').val(res.data.name)
            $("#fPrice").maskMoney('mask', res.data.price);
            $('#fStock').val(res.data.stock)
            $('#fUnitName').val(res.data.unit_name)
            $("#fPhoto").fileinput();
            if (res.data.photo) {
                $("#fPhoto").fileinput('destroy').fileinput({
                    initialPreview: [`<img src='/` + res.data.photo +`' class='file-preview-image' width='120' >`],
                    overwriteInitial: true,
                    initialPreviewConfig: [{ caption: res.data.photo}]
                 })   
            }
        })
    }

    const modalDelete = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    method: "POST",
                    url: '<?= route('product.delete') ?>',
                    data: "id=" + id
                })
                .done((res) => {
                    Swal.fire(
                        'Deleted!',
                        'Your data has been deleted.',
                        'success'
                    ) 
                    loadData();
                })
            }
        })
    }

    //Created
    loadData();
    $("#fPrice").maskMoney();

    $("#myForm").submit((e) => {
        e.preventDefault();
        let title = $("#modal-title").html()
        let url = title == 'Edit Product' ? '<?= route('product.update') ?>' : '<?= route('product.create') ?>'

        let data = new FormData();
        data.append('id', $('#fId').val())
        data.append('name', $('#fName').val())
        data.append('photo', $('#fPhoto').prop('files')[0])
        data.append('price', $("#fPrice").maskMoney('unmasked')[0])
        data.append('stock', $('#fStock').val())
        data.append('unit_name', $('#fUnitName').val())

        $.ajax({
            method: "POST",
            url,
            data,
            processData: false,
            contentType: false,
        })
        .done((res) => {
            $('#modal-item').modal('hide')
            loadData();
        });
    })
</script>

@endsection

