@extends('master')
@section('title', 'Cashier')
@section('content')
<section class="section">
    <div class="section-body">
        <div class="card">
            <div class="card-header">
                <h4>@yield('title')</h4>
            </div>
            <div class="card-body">
               <a onClick="modalAdd()" class="btn btn-primary mb-3">Add</a>
               <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="table-active" class="table" style="width: 100%;"></table>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-item">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="myForm">
            <div class="modal-body">
                <input type="text" class="form-control" name="id" id="fId" hidden>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" id="fName">
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username" id="fUsername">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" id="fPassword">
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" name="password_confirmation" id="fCPassword">
                </div>

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('js')

<script>
    const loadData = () => {
        $('#table-active').DataTable( {
                destroy: true,
                scroller: true,
                scrollX: 200,
                ajax: {
                    url: "<?= route('cashier.table') ?>",
                    method: "GET",
                },
                columns: [
                    {
                        data: "name",
                        title: "Name"
                    },
                    {
                        data: "username",
                        title: "Username"
                    },
                    {
                        title: 'Action',
                        data: 'id'
                    }
                ],
                rowCallback: function(row, data) {
                    $('td:eq(2)', row).html(`
                        <a onClick="modalEdit(`+ data['id'] +`)"><i class="fas fa-pen text-info"></i></a>
                        <a onClick="modalDelete(`+ data['id'] +`)"><i class="fas fa-trash text-danger"></i></a>
                    `);
                },
        } );
    }

    const modalAdd = () => {
        $('#modal-item').modal('show')
        $('#modal-title').html('Add Cashier')

        $('#fName').prop('disabled', false)
        $('#fUsername').prop('disabled', false)

        $('#fName').val(null)
        $('#fUsername').val(null)
        $('#fPassword').val(null)
        $('#fCPassword').val(null)
    }

    const modalEdit = (id) => {
        $.ajax({
            url: "<?= route('cashier') ?>" + "/detail/" + id,
        })
        .done((res) => {
            $('#modal-item').modal('show')
            $('#modal-title').html('Edit Cashier')

            $('#fName').prop('disabled', true)
            $('#fUsername').prop('disabled', true)

            $('#fId').val(res.data.id)
            $('#fName').val(res.data.name)
            $('#fUsername').val(res.data.username)
            $('#fPassword').val(null)
            $('#fCPassword').val(null)
        })
    }

    const modalDelete = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    method: "POST",
                    url: '<?= route('cashier.delete') ?>',
                    data: "id=" + id
                })
                .done((res) => {
                    Swal.fire(
                        'Deleted!',
                        'Your data has been deleted.',
                        'success'
                    ) 
                    loadData();
                })
            }
        })
    }

    //Created
    loadData();

    $("#myForm").submit((e) => {
        e.preventDefault();
        let title = $("#modal-title").html()
        let url = title == 'Edit Cashier' ? '<?= route('cashier.update') ?>' : '<?= route('cashier.create') ?>'

        let data = $("#myForm").serialize();
        
        $.ajax({
            method: "POST",
            url,
            data
        })
        .done((res) => {
            $('#modal-item').modal('hide')
            loadData();
        });
    })
</script>

@endsection

