@extends('master')
@section('title', 'Home')
@section('css')
<style>
#chart {
  max-width: 1100px;
  margin: 35px auto;
}
</style>
@endsection
@section('content')
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Transaction last month</h4>
                        </div>
                        <div class="card-body" id="cTrx">
                            0
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-tags"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Product</h4>
                        </div>
                        <div class="card-body" id="cProduct">
                            0
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Cashier</h4>
                        </div>
                        <div class="card-body" id="cCashier">
                            5
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    
        <div class="card">
            <div class="card-header">
                <h4>Transaction</h4>
            </div>
            <div class="card body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

<script>

var options = {
  chart: {
    height: 380,
    width: "100%",
    type: "area",
    animations: {
      initialAnimation: {
        enabled: false
      }
    }
  },
  theme: {
    palette: 'palette2'
  },
  series: [
    {
      name: "",
      data: []
    }
  ],
  xaxis: {
    type: "datetime"
  },
  yaxis: {
    labels: {
      formatter: function (value) {
        return numberWithCommas(value);
      }
    },
  },
};

const loadChart = () => {
  $.ajax({
      url: "<?= route('home.chart') ?>",
  }).done((res) => {
    chart.updateSeries([{
      name: "Total",
      data: res.data
    }])
  })
}

const loadDashboard = () => {
  $.ajax({
      url: "<?= route('home.count') ?>",
  }).done((res) => {
      $("#cTrx").html(numberWithCommas(res.data.cTrx))
      $("#cProduct").html(res.data.cProduct)
      $("#cCashier").html(res.data.cCashier)
  })
}

const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var chart = new ApexCharts(document.querySelector("#chart"), options);
chart.render();

loadChart();
loadDashboard();

</script>

@endsection