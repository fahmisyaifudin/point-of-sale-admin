require('./bootstrap');

window.$ = window.jQuery = require('jquery');
require( 'datatables.net' );
require( 'datatables.net-dt' );
window.DataTable = require('datatables.net-bs4');
require('bootstrap');
require('popper.js')
require('jquery.nicescroll')

window.Swal = require('sweetalert2');
window.toastr = require('toastr');