<?php

use App\Http\Controllers\CashierController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function(){
    Route::get('/',[HomeController::class, 'index']);

    Route::get('/home/count',[HomeController::class, 'count'])->name('home.count');
    Route::get('/home/chart',[HomeController::class, 'chart'])->name('home.chart');
    
    Route::get('/cashier',[CashierController::class, 'index'])->name('cashier');
    Route::get('/cashier/detail/{id}',[CashierController::class, 'detail'])->name('cashier.detail');
    Route::get('/cashier/table',[CashierController::class, 'table'])->name('cashier.table');
    Route::post('/cashier/create',[CashierController::class, 'create'])->name('cashier.create');
    Route::post('/cashier/update',[CashierController::class, 'update'])->name('cashier.update');
    Route::post('/cashier/delete',[CashierController::class, 'delete'])->name('cashier.delete');

    Route::get('/product',[ProductController::class, 'index'])->name('product');
    Route::get('/product/detail/{id}',[ProductController::class, 'detail'])->name('product.detail');
    Route::get('/product/table',[ProductController::class, 'table'])->name('product.table');
    Route::post('/product/create',[ProductController::class, 'create'])->name('product.create');
    Route::post('/product/update',[ProductController::class, 'update'])->name('product.update');
    Route::post('/product/delete',[ProductController::class, 'delete'])->name('product.delete');

    Route::get('/transaction',[TransactionController::class, 'index'])->name('transaction');
    Route::get('/transaction/table',[TransactionController::class, 'table'])->name('transaction.table');
    Route::get('/transaction/detail/{id}',[TransactionController::class, 'detail'])->name('transaction.detail');
    Route::get('/transaction/download',[TransactionController::class, 'download'])->name('transaction.download');

});

Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');